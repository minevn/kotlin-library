plugins {
	id("com.github.johnrengelman.shadow") version "7.1.2"
}


repositories {
	maven("https://oss.sonatype.org/content/repositories/snapshots")
	maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
}

dependencies {
	compileOnly("org.spigotmc:spigot-api:1.12.2-R0.1-SNAPSHOT")
	compileOnly("net.md-5:bungeecord-api:1.12-SNAPSHOT")
}

tasks {
	val jarName = "kotlin-stdlib"

	register("customCopy") {
		dependsOn(shadowJar)

		val path = project.properties["shadowPath"]
		if (path != null) {
			doLast {
				println(path)
				copy {
					from("build/libs/$jarName.jar")
					into(path)
				}
				println("Copied")
			}
		}
	}

	shadowJar {
		archiveFileName.set("$jarName.jar")
	}

	assemble {
		dependsOn(shadowJar, get("customCopy"))
	}
}
